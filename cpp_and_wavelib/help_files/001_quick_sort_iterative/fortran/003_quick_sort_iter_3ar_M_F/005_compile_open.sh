#!/bin/bash

# 1. compile

  openf95 -fullwarn                   \
          -O3                         \
          m_1_type_definitions.f90    \
          m_2_quick_sort_iter_3ar.f90 \
          driver_program.f90          \
          -o x_open

# 2. clean

  rm *.mod
