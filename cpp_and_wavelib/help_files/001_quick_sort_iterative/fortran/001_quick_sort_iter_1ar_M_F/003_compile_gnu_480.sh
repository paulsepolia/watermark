#!/bin/bash

# 1. compile

  gfortran-4.8 -O3                      \
               -std=f2008               \
               -Wall                    \
               -static                  \
               m_1_type_definitions.f90 \
               m_2_quick_sort_iter.f90  \
               driver_program.f90       \
               -o x_gnu_480

# 2. clean

  rm *.mod
