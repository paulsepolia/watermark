!===================================!
! author: pavlos g. galiatsatos     !
! date: 2013/09/25                  !
! program: the quick sort algorithm !
!===================================!

  program driver_program

  use m_1_type_definitions
  use m_2_quick_sort_iter_2ar

  implicit none

  !  1 --> interface starts

  integer(kind=di), parameter :: DIMEN = 1000000_di

  !  2 --> interface ends

  real(kind=dr), allocatable, dimension(:) :: array1
  real(kind=dr), allocatable, dimension(:) :: array2
  integer(kind=di) :: i
  real(kind=dr) :: t1
  real(kind=dr) :: t2
  real(kind=dr) :: sin

  !  3 --> allocate RAM

  allocate(array1(1:DIMEN))
  allocate(array2(1:DIMEN))

  !  4 --> create the array to be sorted
  !        and the array to follow the sorting

  do i = 1, DIMEN

    array1(i) = real(1+DIMEN-i, kind=dr)
    array2(i) = sin(real(i-1, kind=dr))

  end do

  !  5 --> some unsorted elements

  write(*,*) 
  write(*,*) "  1 --> The Quick Sort algorithm"
  write(*,*) "  2 --> Iterative version: Fast"
  write(*,*) "  3 --> The first 5 unsorted elements:"
  write(*,*)

  do i = 1, 5

    write(*,*)  i
    write(*,*)  array1(i), array2(i)
    write(*,*)

  end do

  write(*,*)
  write(*,*) "  4 --> Sorting. Please wait..."

  !  6 --> main benchmark
 
  call cpu_time(t1)

  call quick_sort_iter_2ar(array1, array2, DIMEN)
 
  call cpu_time(t2)

  !  7 --> outputs

  write(*,*) "  5 --> The first 5 sorted elements:"
  write(*,*)

  do i = 1, 5

    write(*,*) i
    write(*,*) array1(i), array2(i)
    write(*,*)

  end do

  write(*,*)
  write(*,*) "  6 --> The last 5 sorted elements:"
  write(*,*)

  do i = DIMEN-5, DIMEN

    write(*,*) i
    write(*,*) array1(i), array2(i)
    write(*,*)

  end do
  
  write(*,*) "  7 --> Total real time: ", (t2-t1)
  write(*,*) "  8 --> Total sorted elements: ", DIMEN
  write(*,*)

  end program driver_program

! FINI

