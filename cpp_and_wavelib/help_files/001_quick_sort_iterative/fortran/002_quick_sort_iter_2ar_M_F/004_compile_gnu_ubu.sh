#!/bin/bash

# 1. compile

  gfortran  -O3                         \
            -std=f2008                  \
            -Wall                       \
            -static                     \
            m_1_type_definitions.f90    \
            m_2_quick_sort_iter_2ar.f90 \
            driver_program.f90          \
            -o x_gnu_ubu

# 2. clean

  rm *.mod
