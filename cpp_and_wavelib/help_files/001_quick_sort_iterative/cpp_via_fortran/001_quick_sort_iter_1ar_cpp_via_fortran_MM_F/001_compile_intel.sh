#!/bin/bash

# 1. compile fortran

  ifort  -c                      \
	 -O3                     \
         -warn all               \
         -heap-arrays            \
         quick_sort_iter_1ar.f90

# 2. compile c++

  icpc  -c                 \
	-O3                \
        driver_program.cpp

# 3. link c++ and fortran objects

  icpc -O3                   \
       quick_sort_iter_1ar.o \
       driver_program.o      \
       -static               \
       -lifcore              \
       -o x_intel

# 4. clean

  rm *.o
  rm quick_sort_iter_1ar__genmod.f90
  rm *.mod

