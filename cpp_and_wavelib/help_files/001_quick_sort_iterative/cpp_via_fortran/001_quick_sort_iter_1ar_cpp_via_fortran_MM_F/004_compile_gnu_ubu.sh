#!/bin/bash

# 1. compile fortran

  gfortran  -c                      \
	    -O3                     \
            -std=f2008              \
            -Wall                   \
            quick_sort_iter_1ar.f90

# 2. compile c++

  g++  -c                 \
       -O3                \
       driver_program.cpp

# 3. link c++ and fortran objects

  g++  -O3                   \
       quick_sort_iter_1ar.o \
       driver_program.o      \
       -static               \
       -lgfortran            \
       -o x_gnu_ubu

# 4. clean

  rm *.o

