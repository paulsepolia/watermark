#!/bin/bash

# 1. compile fortran

  gfortran-4.8  -c         \
		-O3        \
                -std=f2008 \
                -Wall      \
                quick_sort_iter_3ar.f90

# 2. compile c++

  g++-4.8  -c  \
           -O3 \
           driver_program.cpp

# 3. link c++ and fortran objects

  g++-4.8  -O3                   \
	   quick_sort_iter_3ar.o \
           driver_program.o      \
	   -static               \
	   -lgfortran            \
           -o x_gnu_480

# 4. clean

  rm *.o

