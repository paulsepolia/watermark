     
  subroutine quick_sort_iter_3ar(arr, brr, crr, n)

  implicit none
 
  ! --> type parameters
  
  integer, parameter :: dr = selected_real_kind(p=15,r=300)
  integer, parameter :: sr = selected_real_kind(p=6,r=30)
  integer, parameter :: di = selected_int_kind(18)
  integer, parameter :: si = selected_int_kind(9)

  ! --> interface parameters

  integer(kind=di) :: n
  real(kind=dr), dimension(1:n) :: arr
  real(kind=dr), dimension(1:n) :: brr
  real(kind=dr), dimension(1:n) :: crr

  ! --> local variables

  integer(kind=di), parameter :: m = 7_di
  integer(kind=di), parameter :: nstack = 5000_di
  integer(kind=di) :: i
  integer(kind=di) :: ir
  integer(kind=di) :: j 
  integer(kind=di) :: jstack
  integer(kind=di) :: k
  integer(kind=di) :: l
  integer(kind=di), allocatable, dimension(:) :: istack
  real(kind=dr) :: a
  real(kind=dr) :: b
  real(kind=dr) :: c
  real(kind=dr) :: temp

  ! --> main execution body
 
  allocate(istack(1:NSTACK))

      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          b=brr(j)
          c=crr(j)
          do 11 i=j-1,l,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
            brr(i+1)=brr(i)
            crr(i+1)=crr(i)
11        continue
          i=l-1
2         arr(i+1)=a
          brr(i+1)=b
          crr(i+1)=c
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        temp=brr(k)
        brr(k)=brr(l+1)
        brr(l+1)=temp
        temp=crr(k)
        crr(k)=crr(l+1)
        crr(l+1)=temp
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
          temp=brr(l)
          brr(l)=brr(ir)
          brr(ir)=temp
          temp=crr(l)
          crr(l)=crr(ir)
          crr(ir)=temp
        endif
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
          temp=brr(l+1)
          brr(l+1)=brr(ir)
          brr(ir)=temp
          temp=crr(l+1)
          crr(l+1)=crr(ir)
          crr(ir)=temp
        endif
        if(arr(l).gt.arr(l+1))then
          temp=arr(l)
          arr(l)=arr(l+1)
          arr(l+1)=temp
          temp=brr(l)
          brr(l)=brr(l+1)
          brr(l+1)=temp
          temp=crr(l)
          crr(l)=crr(l+1)
          crr(l+1)=temp
        endif
        i=l+1
        j=ir
        a=arr(l+1)
        b=brr(l+1)
        c=crr(l+1)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        temp=brr(i)
        brr(i)=brr(j)
        brr(j)=temp
        temp=crr(i)
        crr(i)=crr(j)
        crr(j)=temp

        goto 3
5       arr(l+1)=arr(j)
        arr(j)=a
        brr(l+1)=brr(j)
        brr(j)=b
        crr(l+1)=crr(j)
        crr(j)=c
        jstack=jstack+2
        if(jstack.gt.NSTACK) stop 'NSTACK too small in sort2'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1

    deallocate(istack)

  end subroutine quick_sort_iter_3ar

!======!
! FINI !
!======!
