#!/bin/bash

# 1. compile fortran

  gfortran-4.9.1  -c         \
 	          -O3        \
                  -std=f2008 \
                  -Wall      \
                  quick_sort_iter_2ar.f90

# 2. compile c++

  g++-4.9.1  -c  \
             -O3 \
             driver_program.cpp

# 3. link c++ and fortran objects

  g++-4.9.1  -O3                    \
	     quick_sort_iter_2ar.o  \
             driver_program.o       \
	     -L/usr/lib/ -lgfortran \
             -o x_gnu_491

# 4. clean

  rm *.o

