#!/bin/bash

# fortran part --> 1/1

  ifort  -c -O3              \
  	 -openmp             \
  	 -xHost              \
  	 fortran_omp_sub.f90
 
# cpp part --> 1/2

  icpc  -c -O3                      \
  	-openmp                     \
  	-xHost                      \
  	sum_omp_cpp_via_fortran.cpp

# cpp part --> 2/2

  icpc  -O3                       \
        -static                   \
        -openmp                   \
        -xHost                    \
        fortran_omp_sub.o         \
        sum_omp_cpp_via_fortran.o \
        -o x_intel

# cleaning the produced objects

  rm *.o

# exiting

  exit

