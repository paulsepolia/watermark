#!/bin/bash

# fortran part --> 1/1

  gfortran-4.8  -c -O3              \
  	        -fopenmp            \
  	        fortran_omp_sub.f90
 
# cpp part --> 1/2

  g++-4.8  -c -O3                      \
  	   -fopenmp                    \
  	   sum_omp_cpp_via_fortran.cpp

# cpp part --> 2/2

  g++-4.8  -O3                               \
           -fopenmp                          \
	   -static                           \
           fortran_omp_sub.o                 \
           sum_omp_cpp_via_fortran.o         \
           /opt/gcc/491/lib64/libgfortran.a  \
           -o x_gnu_480

# cleaning the produced objects

  rm *.o

# exiting

  exit

