#!/bin/bash

  # fortran part --> 1/1

  ifort  -c -O3               	   \
 	 -static                   \
  	 -openmp                   \
  	 -xHost                    \
  	 fortran_add_mat.f90
 
  # cpp part --> 1/2

  icpc  -c -O3                      \
  	-static                     \
  	-openmp                     \
  	-xHost                      \
  	driver_program.cpp

  # cpp part --> 2/2

  icpc  -O3                       \
  	-static                   \
  	-openmp                   \
  	-xHost                    \
  	fortran_add_mat.o         \
  	driver_program.o          \
  	-o x_intel

  # cleaning the produced objects

  rm *.o

  # exiting

  exit

