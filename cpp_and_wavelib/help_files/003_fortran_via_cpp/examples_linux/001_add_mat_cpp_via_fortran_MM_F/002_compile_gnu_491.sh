#!/bin/bash

  # fortran part --> 1/1

  gfortran-4.9.1  -c -O3              \
 	 	  -static             \
  	 	  -fopenmp            \
  	 	  fortran_add_mat.f90
 
  # cpp part --> 1/2

  g++-4.9.1  -c -O3              \
  	     -static             \
  	     -fopenmp            \
  	     driver_program.cpp

  # cpp part --> 2/2

  g++-4.9.1  -O3                \
  	     -static            \
  	     -fopenmp           \
  	     fortran_add_mat.o  \
  	     driver_program.o   \
  	     -o x_gnu_491

  # cleaning the produced objects

  rm *.o

  # exiting

  exit

