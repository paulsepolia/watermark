#!/bin/bash

  # fortran part --> 1/1

  gfortran  -c -O3              \
 	    -static             \
  	    -fopenmp            \
  	    fortran_add_mat.f90
 
  # cpp part --> 1/2

  g++  -c -O3             \
       -static            \
       -fopenmp           \
       driver_program.cpp

  # cpp part --> 2/2

  g++  -O3               \
       -static           \
       -fopenmp          \
       fortran_add_mat.o \
       driver_program.o  \
       -o x_gnu_ubu

  # cleaning the produced objects

  rm *.o

  # exiting

  exit

