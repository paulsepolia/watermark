#!/bin/bash

# 1. Fortran object

  ifort -c                     \
        -O3                    \
        -warn all              \
        -stand f08             \
        -static                \
        -parallel              \
        -par-threshold0        \
        m_1_fortran_matmul.f90

# 2. C++ interface

  icpc -c                       \
       -Wall                    \
       -O3                      \
       -static                  \
       -std=c++11               \
       -parallel                \
       -par-threshold0          \
       m_2_fortran_matmul_cpp.h

# 3. C++ main program
  
  icpc -O3                         \
       -Wall                       \
       -static                     \
       -std=c++11                  \
       -parallel                   \
       -par-threshold0             \
       m_1_fortran_matmul.o        \
       m_2_fortran_matmul_cpp.o    \
       m_3_fortran_matmul_main.cpp \
       -lifcore                    \
       -lgfortran                  \
       -o x_intel

# 4.

  rm *.o
  rm *.mod
  rm fortran_matmul__genmod.f90

