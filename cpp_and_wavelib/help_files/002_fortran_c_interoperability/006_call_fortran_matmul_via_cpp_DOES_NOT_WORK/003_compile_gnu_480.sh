#!/bin/bash

# 1. Fortran object

  gfortran-4.8  -c                     \
                -O3                    \
                -Wall                  \
                -std=f2008             \
                -static                \
                m_1_fortran_matmul.f90

# 2. C++ main program
  
  g++-4.8  -O3                         \
           -Wall                       \
           -static                     \
           -ansi                       \
           m_1_fortran_matmul.o        \
           m_3_fortran_matmul_main.cpp \
           -L/usr/lib -lgfortran       \
           -o x_gnu_480

# 4.

  rm *.o

