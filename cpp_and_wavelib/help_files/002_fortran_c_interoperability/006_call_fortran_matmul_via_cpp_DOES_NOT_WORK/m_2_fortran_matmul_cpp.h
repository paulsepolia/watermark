//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/07/11              //
//===============================//

#ifndef M_2_FORTRAN_MATMUL_CPP_H
#define M_2_FORTRAN_MATMUL_CPP_H
extern "C"
{
  void fortran_matmul_(double **, double **, double **, const int*, const int*);
}
#endif

//======//
// Fini //
//======//
