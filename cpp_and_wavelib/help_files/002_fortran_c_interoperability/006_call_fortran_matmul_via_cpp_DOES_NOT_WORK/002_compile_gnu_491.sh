#!/bin/bash

# 1. Fortran object

  gfortran-4.9.1  -c                     \
                  -O3                    \
                  -Wall                  \
                  -std=f2008             \
                  -static                \
                  m_1_fortran_matmul.f90

# 3. C++ main program
  
  g++-4.9.1 -O3                         \
            -Wall                       \
            -ansi                       \
	    -fPIC                       \
            m_1_fortran_matmul.o        \
            m_3_fortran_matmul_main.cpp \
	    /opt/gcc/491/lib64/*.so     \
            -o x_gnu_491

# 4.

  rm *.o

