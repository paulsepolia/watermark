#!/bin/bash

# 1. Fortran object

  gfortran  -c                     \
            -O3                    \
            -Wall                  \
            -std=f2008             \
            -static                \
            m_1_fortran_matmul.f90

# 3. C++ main program
  
  g++  -O3                         \
       -Wall                       \
       -static                     \
       -ansi                       \
       m_1_fortran_matmul.o        \
       m_3_fortran_matmul_main.cpp \
       -L/usr/lib -lgfortran       \
       -o x_gnu_ubu

# 4.

  rm *.o
