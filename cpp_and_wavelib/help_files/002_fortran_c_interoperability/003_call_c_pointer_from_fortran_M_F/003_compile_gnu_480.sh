#!/bin/bash

# 1. fortran object

  gfortran-4.8  -c                 \
                -O3                \
                -Wall              \
                -std=f2008         \
                -static            \
                m_1_parameters.f90


# 2. C++ object

  gcc-4.8  -c              \
           -Wall           \
           -O3             \
           -static         \
           -std=c99        \
           m_2_c_pointer.c


# 3. Fortran main program
  
  gfortran-4.8  -O3                       \
                -Wall                     \
                -static                   \
                -std=f2008                \
                m_1_parameters.o          \
                m_2_c_pointer.o           \
                m_3_c_fortran_pointer.f90 \
                -o x_gnu_480

# 4.

  rm *.o
  rm *.mod

