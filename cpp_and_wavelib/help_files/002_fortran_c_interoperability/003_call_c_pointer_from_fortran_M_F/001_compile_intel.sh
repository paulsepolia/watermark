#!/bin/bash

# 1. fortran object

  ifort  -c                 \
         -O3                \
         -warn all          \
         -stand f08         \
         -static            \
         m_1_parameters.f90


# 2. C++ object

  icc  -c               \
       -Wall            \
       -O3              \
       -static          \
       -std=c99         \
       -parallel        \
       -par-threshold0  \
       m_2_c_pointer.c


# 3. Fortran main program
  
  ifort -O3                       \
        -warn all                 \
        -static                   \
        -stand f08                \
        -parallel                 \
        -par-threshold0           \
        m_1_parameters.o          \
        m_2_c_pointer.o           \
        m_3_c_fortran_pointer.f90 \
        -o x_intel

# 4.

  rm *.o
  rm *.mod
