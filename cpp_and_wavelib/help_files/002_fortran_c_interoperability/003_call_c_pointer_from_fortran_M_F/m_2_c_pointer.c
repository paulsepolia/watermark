//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 20134/10/21             //
//===============================//

#include <stdlib.h>
#include <stdio.h>

int * c_allocate(int n)
{
	// local varaibles  

  	int * ptr;
  
  	// local RAM allocation

  	ptr = (int*) malloc(n*sizeof(int));

  	// initialization

  	for (int i = 0; i < n; i++)
  	{  
		ptr[i] = i;  
	}

	// print 

  	printf("  --> c_allocate: n = %d\n", n);

  	// exit

  	return ptr;
}

//======//
// FINI //
//======//
