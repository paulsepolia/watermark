!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/07/10              !
!===============================!

  program char_test
 
  use m_1_parameters, only: si, di, sr, dr
  use iso_c_binding, only: C_CHAR, C_NULL_CHAR
  
  implicit none

  ! 1. subroutine interface

  interface
    subroutine c_routine(string) bind(C, name='c_routine')
      import C_CHAR
      implicit none
      character(kind=C_CHAR), intent(in) :: string(*)
    end subroutine c_routine
  end interface

  ! 2. local variables 

  integer(kind=si)                           :: fstring_len
  integer(kind=si)                           :: alloc_stat
  integer(kind=si)                           :: i_char
  character(len=:), allocatable              :: fstring
  character(kind=C_CHAR, len=1), allocatable :: cstring(:)  

  ! 3. main code
  !    Converts a character string into an array of
  !    characters, terminated with a null sentinel character.

  ! 3.1 --> initialization

  fstring = "Hello C world from Fortran"
  fstring_len = len_trim(fstring)
  
  ! 3.2 --> allocation

  allocate (cstring(fstring_len+1), stat=alloc_stat)

  ! 3.3 --> assingment

  forall(i_char = 1 : fstring_len)
    cstring(i_char) = fstring(i_char : i_char)
  end forall
  
  cstring(fstring_len + 1) = C_NULL_CHAR

  ! 3.4 --> call C function

  call c_routine(cstring)

  end program char_test

!======!
! Fini !
!======!
