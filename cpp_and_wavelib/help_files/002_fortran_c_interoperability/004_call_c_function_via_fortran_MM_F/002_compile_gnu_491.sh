#!/bin/bash

# 1. Fortran object

  gfortran-4.9.1  -c                 \
                  -O3                \
                  -Wall              \
                  -std=f2008         \
                  -static            \
                  m_1_parameters.f90

# 2. C object

  gcc-4.9.1  -c              \
             -Wall           \
             -O3             \
             -static         \
             -std=c99        \
             m_2_c_routine.c

# 3. Fortran main program
  
  gfortran-4.9.1  -O3                      \
                  -Wall                    \
                  -static                  \
                  -std=f2008               \
                  m_1_parameters.o         \
                  m_2_c_routine.o          \
                  m_3_pass_string_to_c.f90 \
                  -o x_gnu_491

# 4.

  rm *.o
  rm *.mod

