#!/bin/bash

# 1 --> Fortran object

  gfortran  -c                      \
            -O3                     \
            -Wall                   \
            -std=f2008              \
            -static                 \
	    -fopenmp                \
            m_1_fortran_vec_add.f90

# 2 --> C++ main program
  
  g++  -O3                   \
       -Wall                 \
       -static               \
       -ansi                 \
       -fopenmp              \
       m_1_fortran_vec_add.o \
       driver_program.cpp    \
       -o x_gnu_ubu

# 3 --> clean up

  rm *.o

