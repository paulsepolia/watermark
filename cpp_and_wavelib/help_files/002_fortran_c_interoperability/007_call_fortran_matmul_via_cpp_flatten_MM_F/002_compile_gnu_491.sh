#!/bin/bash

# 1. Fortran object

  gfortran-4.9.1  -c                     \
                  -O3                    \
                  -Wall                  \
                  -std=f2008             \
                  -static                \
	          -fopenmp               \
                  m_1_fortran_matmul.f90

# 2. C++ main program
  
  g++-4.9.1 -O3                     \
            -Wall                   \
            -ansi                   \
	    -fPIC                   \
            m_1_fortran_matmul.o    \
            driver_program.cpp      \
	    /opt/gcc/491/lib64/*.so \
	    -fopenmp                \
            -o x_gnu_491

# 3.

  rm *.o

