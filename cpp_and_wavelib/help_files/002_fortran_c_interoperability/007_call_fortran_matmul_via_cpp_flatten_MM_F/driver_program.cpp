//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/07/11              //
//===============================//

#include <iostream>
#include <ctime>
#include <omp.h>
#include <iomanip>

#include "m_2_fortran_matmul_cpp.h"

using std::cin;
using std::cout;
using std::endl;
using std::clock;
using std::setprecision;
using std::fixed;

// the main function

int main()
{
	// --> parameters

  	const long ROWS = 2000; // ROWS must be equal to COLS

	// --> rest parameters

	const long COLS = ROWS;
	const long TOT_ELEM = COLS * ROWS;

	// --> variables

	long i;
	long j;
	clock_t t1;
	clock_t t2;
	int nt;

	// --> adjust the output format

	cout << fixed;
	cout << setprecision(20);

	// --> allocate RAM
	// --> treat matrices as vectors

	cout << " --> 1 --> allocate RAM" << endl;

	double * mat_a = new double [TOT_ELEM];
  	double * mat_b = new double [TOT_ELEM];
  	double * mat_c = new double [TOT_ELEM];

  	// --> matrix initialization

	cout << " --> 2 --> build the matrices" << endl;

        #pragma omp parallel default(none) \
			     private(i)    \
			     private(j)    \
			     shared(mat_a) \
			     shared(mat_b) \
			     shared(mat_c)
	{
		#pragma omp for

  		for(i = 0; i < ROWS; ++i)
  		{
    			for(j = 0; j < COLS; ++j)
    			{
      				mat_a[i*COLS+j] = static_cast<double>(1.0);
      				mat_b[i*COLS+j] = static_cast<double>(1.0);
      				mat_c[i*COLS+j] = static_cast<double>(0.0);
    			}
  		}
	}

	// --> get the number of openmp threads

	#pragma omp parallel default(none) shared(nt)
	{
		nt = omp_get_num_threads();
	}

  	// --> call the fortran defined function

  	cout << " --> 3 --> call the fortran defined function " << endl;

	t1 = clock();

  	fortran_matmul_(mat_a, mat_b, mat_c, &ROWS, &COLS);

	t2 = clock();

	cout << " --> 4 --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC/nt << endl;

  	cout << " --> 5 --> some output..." << endl;

	// --> some output

  	cout << " mat_c[0]          = " << mat_c[0] << endl;
  	cout << " mat_c[1]          = " << mat_c[1] << endl;
  	cout << " mat_c[2]          = " << mat_c[2] << endl;
  	cout << " mat_c[3]          = " << mat_c[3] << endl;
  	cout << " mat_c[TOT_ELEM-1] = " << mat_c[TOT_ELEM-1] << endl;
  	cout << " mat_c[TOT_ELEM-2] = " << mat_c[TOT_ELEM-2] << endl;

  	// --> deallocation

	cout << " --> 6 --> free up the RAM" << endl;
 
  	delete [] mat_a;
  	delete [] mat_b;
  	delete [] mat_c;

	// XX --> sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

  	return 0;
}

//======//
// FINI //
//======//

