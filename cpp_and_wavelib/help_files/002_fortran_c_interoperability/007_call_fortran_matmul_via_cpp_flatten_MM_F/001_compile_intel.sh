#!/bin/bash

# 1. Fortran object

  ifort -c                     \
        -O3                    \
        -warn all              \
        -stand f08             \
        -static                \
	-openmp                \
        m_1_fortran_matmul.f90

# 2. C++ main program
  
  icpc -O3                  \
       -Wall                \
       -static              \
       -std=c++11           \
       m_1_fortran_matmul.o \
       driver_program.cpp   \
       -lifcore             \
       -lgfortran           \
       -openmp              \
       -o x_intel

# 3.

  rm  *.o
  rm  *.mod
  rm  fortran_matmul__genmod.f90

