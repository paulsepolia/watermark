#!/bin/bash

# 1. Fortran object

  gfortran  -c                     \
            -O3                    \
            -Wall                  \
            -std=f2008             \
            -static                \
	    -fopenmp               \
            m_1_fortran_matmul.f90

# 3. C++ main program
  
  g++  -O3                   \
       -Wall                 \
       -static               \
       -ansi                 \
       m_1_fortran_matmul.o  \
       driver_program.cpp    \
       -L/usr/lib -lgfortran \
       -fopenmp              \
       -o x_gnu_ubu

# 4.

  rm *.o
