#!/bin/bash

# 1. Fortran object

  gfortran-4.8  -c                     \
                -O3                    \
                -Wall                  \
                -std=f2008             \
                -static                \
	        -fopenmp               \
                m_1_fortran_matmul.f90

# 2. C++ main program
  
  g++-4.8  -O3                   \
           -Wall                 \
           -static               \
           -ansi                 \
           m_1_fortran_matmul.o  \
           driver_program.cpp    \
           -L/usr/lib -lgfortran \
	   -fopenmp              \
           -o x_gnu_480

# 3.

  rm *.o

