!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/10/21              !
!===============================!

  subroutine fortran_matmul(mat_a, &
                            mat_b, &
                            mat_c, &
                            ROWS,  &
                            COLS)

  use iso_c_binding, only: C_DOUBLE, C_LONG

  implicit none

  ! interface variables

  integer(kind=C_LONG), intent(in)                             :: ROWS
  integer(kind=C_LONG), intent(in)                             :: COLS
  real(kind=C_DOUBLE), dimension(0:ROWS*COLS-1), intent(in)    :: mat_a
  real(kind=C_DOUBLE), dimension(0:ROWS*COLS-1), intent(in)    :: mat_b
  real(kind=C_DOUBLE), dimension(0:ROWS*COLS-1), intent(inout) :: mat_c

  ! local variables

  integer(kind=C_LONG) :: ELEM_TOT
  integer(kind=C_LONG) :: i, j, k

  ! execution starts

  ELEM_TOT = ROWS * COLS

  ! a hand-made matrix multiplication
  ! ROWS must be equal to COLS 

  !$omp parallel default(none) &
  !$omp private(i,j,k)         &
  !$omp shared(COLS, ROWS)     &
  !$omp shared(mat_a, mat_b)   &
  !$omp shared(mat_c)

  !$omp do
 
  do j = 0, COLS-1
    do i = 0, ROWS-1
      do k = 0, COLS-1

        mat_c(i*COLS+j) = mat_c(i*COLS+j) + mat_a(i*COLS+k)*mat_b(k*COLS+j)

      enddo
    enddo
  enddo

  !$omp end do
  !$omp end parallel
  

  end subroutine fortran_matmul

!======!
! FINI !
!======!

