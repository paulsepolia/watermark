#!/bin/bash

# 1.

  gfortran-4.9.1  -c                  \
                  -O3                 \
                  -Wall               \
                  -std=f2008          \
                  m_1_pid_printer.f90

# 2.

  gfortran-4.9.1  -O3               \
                  -Wall             \
                  -std=f2008        \
                  -static           \
                  m_1_pid_printer.o \
                  -o x_gnu_491

# 3.

  rm *.o
