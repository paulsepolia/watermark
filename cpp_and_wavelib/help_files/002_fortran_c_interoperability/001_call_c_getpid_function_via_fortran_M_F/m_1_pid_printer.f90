!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/07/10              !
!===============================!

  program pid_printer

  implicit none

  ! 1. Function getpid() and the related coding to make it
  !    equivalent to C function 'getpid'.

  interface
    function getpid_f08() bind(c, name='getpid')
      use iso_c_binding, only: pid_t => C_INT ! rename C_INT to pid_t
      implicit none
      integer(kind=pid_t) :: getpid_f08
    end function getpid_f08
  end interface

  ! 2. A test

  write(*,*) " pid --> ", getpid_f08()

  end program pid_printer

!======!
! Fini !
!======!
