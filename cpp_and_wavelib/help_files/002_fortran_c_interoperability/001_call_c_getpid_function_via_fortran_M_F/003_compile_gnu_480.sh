#!/bin/bash

# 1.

  gfortran-4.8  -c                  \
                -O3                 \
                -Wall               \
                -std=f2008          \
                m_1_pid_printer.f90

# 2.

  gfortran-4.8  -O3               \
                -Wall             \
                -std=f2008        \
                -static           \
                m_1_pid_printer.o \
                -o x_gnu_480

# 3.

  rm *.o
