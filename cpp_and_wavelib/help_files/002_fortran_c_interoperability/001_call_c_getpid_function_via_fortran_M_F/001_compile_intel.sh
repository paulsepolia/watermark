#!/bin/bash

# 1. compile

  ifort -c                  \
        -O3                 \
        -warn all           \
        -stand f08          \
        m_1_pid_printer.f90

# 2. link

  ifort -O3               \
        -warn all         \
        -stand f08        \
        -static           \
        m_1_pid_printer.o \
        -o x_intel

# 3. clean

  rm *.o
