//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/21              //
//===============================//

#ifndef M_2_FORTRAN_DOT_CPP_H
#define M_2_FORTRAN_DOT_CPP_H

extern "C"
{
  	void fortran_dot_(double *,
		    	  double *,
			  double *,
                          const long *);
}

#endif

//======//
// FINI //
//======//
