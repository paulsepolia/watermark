#!/bin/bash

# 1 --> Fortran object

  ifort  -c                  \
         -O3                 \
         -warn all           \
         -stand f08          \
         -static             \
         -parallel           \
         -par-threshold0     \
         m_1_fortran_dot.f90

# 2 --> C++ main program
  
  icpc -O3                   \
       -Wall                 \
       -static               \
       -std=c++11            \
       -parallel             \
       -par-threshold0       \
       -openmp               \
       m_1_fortran_dot.o     \
       driver_program.cpp    \
       -o x_intel

# 3 --> clean

  rm *.o
  rm *.mod
  rm fortran_dot__genmod.f90

