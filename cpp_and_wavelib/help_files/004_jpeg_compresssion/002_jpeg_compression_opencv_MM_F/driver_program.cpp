
//========================================//
// read - write jpeg of various qualities //
// using OpenCV                           //
//========================================//

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

#include "cv.h"
#include "highgui.h"

using namespace cv;

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::setfill;
using std::setw;
using std::stringstream;

// the main function

int main()
{
	// local variables

	int p[3];
    	IplImage * img;
	string fileName;
	const int I_MAX = 101;
	stringstream ss;

    	// load image

    	img = cvLoadImage("input/wm_image_1.bmp");

	// save image in varius jpeg qualities

	for (int i = 0; i != I_MAX; ++i)
	{
		// jpeg output settings

		p[0] = CV_IMWRITE_JPEG_QUALITY;
    		p[1] = i;
    		p[2] = 0;

		// save the image

		ss << setfill('0') << setw(4) << i; // set the stringstring

		fileName = ss.str(); // assign it to a string

		fileName = "output/wm_" + fileName + ".jpg"; // create the file name

    		cvSaveImage(fileName.c_str(), img, p); // save the file

		ss.str(""); // reset the string stream
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

