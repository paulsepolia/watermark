#!/bin/bash

  # 1. compile

  g++-4.9.1  -O3                    \
             -Wall                  \
             -std=gnu++14           \
             driver_program.cpp     \
             /opt/opencv/249/lib/*  \
             -o x_gnu_491
