#!/bin/bash

  # 1. compile

  g++  -O3                    \
       -Wall                  \
       -std=c++0x             \
       driver_program.cpp     \
       /opt/opencv/249/lib/*  \
       -o x_gnu_ubu
