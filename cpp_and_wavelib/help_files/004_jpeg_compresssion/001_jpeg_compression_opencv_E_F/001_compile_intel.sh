#!/bin/bash

  # 1. compile

  icpc  -O3                   \
        -xHost                \
        -Wall                 \
        -std=c++11            \
        -wd2012               \
        driver_program.cpp    \
        /opt/opencv/249/lib/* \
        -o x_intel
