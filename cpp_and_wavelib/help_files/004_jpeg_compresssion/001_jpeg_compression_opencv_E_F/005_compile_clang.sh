#!/bin/bash

  # 1. compile

  clang++  -O3                    \
           -Wall                  \
           -std=c++11             \
           driver_program.cpp     \
           /opt/opencv/249/lib/*  \
           -o x_clang_350
