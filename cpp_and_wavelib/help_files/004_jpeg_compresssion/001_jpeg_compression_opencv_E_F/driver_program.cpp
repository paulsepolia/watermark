
//========================================//
// read - write jpeg of various qualities //
// using OpenCV                           //
//========================================//

#include <iostream>

#include "cv.h"
#include "highgui.h"

using namespace cv;

using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
	// local variables

	int p[3];
    	IplImage * img;

    	// load image

    	img = cvLoadImage("lenaHD.bmp");

	// jpeg output settings 

    	p[0] = CV_IMWRITE_JPEG_QUALITY;
    	p[1] = 10;
    	p[2] = 0;

	// save the image

    	cvSaveImage("lenaHD_jpeg_010.jpg", img, p);

	// jpeg output settings

    	p[0] = CV_IMWRITE_JPEG_QUALITY;
    	p[1] = 100;
    	p[2] = 0;

	// save image

    	cvSaveImage("lenaHD_jpeg_100.jpg", img, p);

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//
