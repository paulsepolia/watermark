#!/bin/bash

  # compile fortran object

  gfortran  -c         \
            -O3        \
            -std=f2008 \
            -Wall      \
            quick_sort_iter_2ar.f90

  # compile c++ object

  clang++  -c         \
           -O3        \
           -std=c++11 \
           driver_program.cpp

  # link c++ and fortran objects

  clang++  -O3                                          \
           -Wall                                        \
           -std=c++11                                   \
           quick_sort_iter_2ar.o                        \
           driver_program.o                             \
           /opt/wavelib/clang_350/lib/libwavelib2s.a    \
           /opt/fftw/334/gnu_ubu/lib/libfftw3_omp.a     \
           /opt/fftw/334/gnu_ubu/lib/libfftw3.a         \
           /opt/fftw/334/gnu_ubu/lib/libfftw3_threads.a \
           /opt/opencv/249/lib/*                        \
           -lm                                          \
           -fopenmp                                     \
           -lpthread                                    \
           -L/usr/lib/ -lgfortran                       \
           -o x_clang_350

  # clean

  rm *.o

