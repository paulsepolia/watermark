#!/bin/bash

  # compile fortran object

  gfortran-4.9.1  -c         \
                  -O3        \
                  -std=f2008 \
                  -Wall      \
                  quick_sort_iter_2ar.f90

  # compile c++ object

  g++-4.9.1  -c           \
             -O3          \
	     -std=gnu++14 \
             driver_program.cpp

  # link c++ and fortran objects

  g++-4.9.1 -O3                                          \
            -Wall                                        \
            -std=gnu++14                                 \
            quick_sort_iter_2ar.o                        \
            driver_program.o                             \
            /opt/wavelib/gnu_491/lib/libwavelib2s.a      \
	    /opt/fftw/334/gnu_480/lib/libfftw3_omp.a     \
            /opt/fftw/334/gnu_480/lib/libfftw3.a         \
            /opt/fftw/334/gnu_480/lib/libfftw3_threads.a \
            /opt/opencv/249/lib/*                        \
            -lm                                          \
            -fopenmp                                     \
            -lpthread                                    \
            -L/usr/lib/ -lgfortran                       \
            -o x_gnu_491

  # clean

  rm *.o

