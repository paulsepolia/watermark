#!/bin/bash

  # 1. compile

  g++-4.9.1 -O3                                     \
            -Wall                                   \
            -std=gnu++14                            \
            driver_program.cpp                      \
            /opt/wavelib/gnu_491/lib/libwavelib2s.a \
	    /opt/fftw/334/gnu_480/lib/libfftw3.a    \
	    /opt/opencv/249/lib/*                   \
            -o x_gnu_491
