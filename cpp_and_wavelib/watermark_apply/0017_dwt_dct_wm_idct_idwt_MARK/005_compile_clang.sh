#!/bin/bash

  # 1. compile

  clang++   -O3                                          \
            -Wall                                        \
            -std=c++11                                   \
            driver_program.cpp                           \
	       /opt/wavelib/clang_350/lib/libwavelib2s.a    \
	       /opt/fftw/334/gnu_ubu/lib/libfftw3_omp.a     \
            /opt/fftw/334/gnu_ubu/lib/libfftw3.a         \
            /opt/fftw/334/gnu_ubu/lib/libfftw3_threads.a \
            /opt/opencv/249/lib/*                        \
            -lm                                          \
            -fopenmp                                     \
            -lpthread                                    \
            -o x_clang_350
