#!/bin/bash

  # 1. compile

  g++-4.8   -O3                                     \
            -Wall                                   \
            -std=c++0x                              \
            driver_program.cpp                      \
            /opt/wavelib/gnu_480/lib/libwavelib2s.a \
	    /opt/fftw/334/gnu_480/lib/libfftw3.a    \
	    /opt/opencv/249/lib/*                   \
            -o x_gnu_480
